package es.mmm.gpsalarm

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import es.mmm.gps_alarm.R
import android.content.Intent



class AlarmActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_alarm)
    }

    fun stopAlarm(view: View){
        val stopIntent = Intent(this, RingtoneService::class.java)
        this.stopService(stopIntent)
    }
}
