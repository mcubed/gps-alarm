package es.mmm.gpsalarm.lines

import android.content.Context
import es.mmm.gps_alarm.BR
import es.mmm.gps_alarm.databinding.ItemLineBinding
import es.mmm.gpsalarm.global.ViewHolderAbstract

class LineHolder(binding: ItemLineBinding): ViewHolderAbstract<Line>(binding){

    override fun bind(context: Context, bindableObject: Line) {
    }

    fun bind(context: Context, actions: LineActions, bindableObject: Line) {
        binding.setVariable(BR.line, bindableObject)
        binding.setVariable(BR.actions, actions)
    }
}