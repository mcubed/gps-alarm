package es.mmm.gpsalarm.lines

interface LineActions {

    fun getStops(lineId: String)
}