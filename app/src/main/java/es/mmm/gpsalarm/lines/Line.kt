package es.mmm.gpsalarm.lines

data class Line(val id: String, val name: String)
