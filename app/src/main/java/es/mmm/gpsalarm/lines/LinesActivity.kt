package es.mmm.gpsalarm.lines

import es.mmm.gps_alarm.R
import es.mmm.gpsalarm.global.GlobalActivity
import android.databinding.DataBindingUtil
import android.support.v7.widget.LinearLayoutManager
import es.mmm.gps_alarm.databinding.ActivityLinesBinding
import es.mmm.gpsalarm.Constants
import es.mmm.gpsalarm.stops.StopsActivity
import es.mmm.gpsalarm.data.MetroData
import org.jetbrains.anko.intentFor


class LinesActivity : GlobalActivity(), LineActions {


    private lateinit var binding: ActivityLinesBinding

    override fun initBinding() {
        binding = DataBindingUtil.setContentView(this@LinesActivity, R.layout.activity_lines)
        val data = MetroData(this)

        //Recycler view
        binding.linesRecycler.layoutManager = LinearLayoutManager(this)
        binding.linesRecycler.adapter = LinesAdapter(this, this, data.getMetroLines())

    }

    override fun getStops(lineId: String) {
        startActivity(intentFor<StopsActivity>(Constants.LINE to lineId))
    }

}
