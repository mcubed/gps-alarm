package es.mmm.gpsalarm.lines

import android.content.Context
import android.databinding.DataBindingUtil
import android.view.LayoutInflater
import android.view.ViewGroup
import es.mmm.gps_alarm.R
import es.mmm.gps_alarm.databinding.ItemLineBinding
import es.mmm.gpsalarm.global.AbstractGlobalAdapter
import es.mmm.gpsalarm.global.ViewHolderAbstract

class LinesAdapter(context: Context, val actions: LineActions, listItems: MutableList<Line>?) :
    AbstractGlobalAdapter<Line>(context, listItems) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolderAbstract<Line> {

        val layoutInflater = LayoutInflater.from(parent.context)
        val binding: ItemLineBinding =
            DataBindingUtil.inflate(layoutInflater, R.layout.item_line,parent,false)

        return LineHolder(binding)

    }
    override fun onBindViewHolder(holder: ViewHolderAbstract<Line>, position: Int) {
        holder as LineHolder
        holder.bind(context, actions,  getItem(position))
    }
}