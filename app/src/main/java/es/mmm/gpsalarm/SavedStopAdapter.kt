package es.mmm.gpsalarm

import android.content.Context
import android.databinding.DataBindingUtil
import android.view.LayoutInflater
import android.view.ViewGroup
import es.mmm.gps_alarm.R
import es.mmm.gpsalarm.global.AbstractGlobalAdapter
import es.mmm.gpsalarm.global.ViewHolderAbstract

class SavedStopsAdapter(context: Context, val actions: SavedStopActions, listItems: MutableList<SavedStop>?) :
    AbstractGlobalAdapter<SavedStop>(context, listItems) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolderAbstract<SavedStop> {

        val layoutInflater = LayoutInflater.from(parent.context)
        val binding: ItemSavedStopBinding =
            DataBindingUtil.inflate(layoutInflater, R.layout.item_saved_stop, parent, false)

        return SavedStopHolder(binding)

    }

    override fun onBindViewHolder(holder: ViewHolderAbstract<SavedStop>, position: Int) {
        holder as SavedStopHolder
        holder.bind(context, actions, getItem(position))
    }
}