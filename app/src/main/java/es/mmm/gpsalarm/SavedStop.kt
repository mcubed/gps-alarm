package es.mmm.gpsalarm

data class SavedStop(val line: String, val stop: String, val active: Boolean)