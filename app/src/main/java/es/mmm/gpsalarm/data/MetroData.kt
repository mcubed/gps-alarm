package es.mmm.gpsalarm.data

import android.content.Context
import com.google.gson.Gson
import es.mmm.gpsalarm.lines.Line


class MetroData(val context: Context) {

    private lateinit var lines: MetroLines
    private lateinit var stops: MetroStops

    private fun getLinesData(): List<MetroLine> {

        if (!::lines.isInitialized) {
            val json = context.assets.open("lines.json").bufferedReader().use {
                it.readText()
            }
            lines = Gson().fromJson(json, MetroLines::class.java)
        }
        return lines.lines
    }

    private fun getStopsData(): List<MetroStop> {

        if (!::stops.isInitialized) {
            val json = context.assets.open("estaciones.json").bufferedReader().use {
                it.readText()
            }
            stops = Gson().fromJson(json, MetroStops::class.java)
        }
        return stops.stops
    }

    fun getMetroLines(): MutableList<Line> {
        return metroLines.toMutableList()
    }

    private val metroLines = listOf(
        Line("1", "Pinar de Chamartín - Valdecarros"),
        Line("2", "Las Rosas - Cuatro Caminos"),
        Line("3", "Villaverde Alto - Moncloa"),
        Line("4", "Argüelles- Pinar de Chamartín"),
        Line("5", "Alameda de Osuna - Casa de Campo"),
        Line("6", "Circular"),
        Line("7", "Hospital del Henares - Aeropuerto T4"),
        Line("9", "Paco de Lucía - Arganda del Rey"),
        Line("10", "Hospital Infanta Sofía - Puerta del Sur"),
        Line("11", "Plaza Elíptica - La Fortuna"),
        Line("12", "MetroSur (circular)"),
        Line("R", "Ópera - Príncipe Pío")
    )

    fun getLineStops(line: String) : ArrayList<MetroStop>{
        val lineStops = arrayListOf<MetroStop>()
        getLinesData().filter { it.attributes.line == line }.forEach {
                lineStop -> lineStops.add(getStop(lineStop.attributes.id))
        }
        return lineStops

    }
    fun getStop(id: String): MetroStop = getStopsData().filter { it.attributes.id == id }[0]
}