package es.mmm.gpsalarm.data

import com.google.gson.annotations.SerializedName

data class Coordinates(
    @SerializedName("x") var longitude: Double,
    @SerializedName("y")var latitude: Double)
