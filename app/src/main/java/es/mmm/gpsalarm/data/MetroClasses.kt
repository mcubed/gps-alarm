package es.mmm.gpsalarm.data

import com.google.gson.annotations.SerializedName

data class MetroLines(val lines: List<MetroLine>)

data class MetroStops(val stops: List<MetroStop>)

data class MetroLine(
    @SerializedName("attributes") var attributes: LineAttributes)

data class LineAttributes(
    @SerializedName("IDFESTACION") var id: String,
    @SerializedName("NUMEROLINEAUSUARIO")var line: String)

data class MetroStop(
    @SerializedName("attributes") var attributes: StopAttributes,
    @SerializedName("geometry")var location: Coordinates)

data class StopAttributes(
    @SerializedName("DENOMINACION") var name: String,
    @SerializedName("IDESTACION")var id: String)
