package es.mmm.gpsalarm.global

import android.Manifest
import android.app.AlertDialog
import android.content.Context
import android.content.pm.ActivityInfo
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationManager
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.support.v7.app.AppCompatDelegate
import android.util.Log
import es.mmm.gps_alarm.R
import org.json.JSONObject

abstract class GlobalActivity : AppCompatActivity(), GlobalActions {

    var userLocation =  Location("me")
    private val PERMISSION_REQUEST = 103

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
        initBinding()
    }

    abstract fun initBinding()

    fun handleError(throwable: Throwable) {
        showMessage("Ha ocurrido un error")
        Log.getStackTraceString(throwable)
    }

    override fun back() {
        supportFinishAfterTransition()
    }
    override fun share() {

    }

    fun showMessage(message: String, title: String?=null) : AlertDialog {
        val builder = AlertDialog.Builder(this)

        // Set the alert dialog title
        builder.setTitle(title)

        // Display a message on alert dialog
        builder.setMessage(message)

        // Set a positive button and its click listener on alert dialog
        builder.setPositiveButton("Aceptar") { dialog, which ->
        }

        // Display a negative button on alert dialog
        builder.setNegativeButton("Cancelar") { dialog, which ->

        }

        val dialog: AlertDialog = builder.create()
        dialog.show()

        return dialog
    }

    override fun search() {
        //startActivity(intentFor<SearchRouteActivity>())
    }


    fun getCurrentLocation() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), PERMISSION_REQUEST)
        } else {
            val lm = getSystemService(Context.LOCATION_SERVICE) as LocationManager
            val lastLocation = lm.getLastKnownLocation(LocationManager.GPS_PROVIDER)
            if (lastLocation != null)
                userLocation = lastLocation
        }
    }
}