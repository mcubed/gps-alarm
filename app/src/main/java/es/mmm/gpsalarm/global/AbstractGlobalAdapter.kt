package es.mmm.gpsalarm.global

import android.content.Context
import android.databinding.ViewDataBinding
import android.support.v7.widget.RecyclerView

abstract class AbstractGlobalAdapter<T>(val context: Context, var listItems: MutableList<T>? = null) :
    RecyclerView.Adapter<ViewHolderAbstract<T>>() {

    override fun getItemCount(): Int = listItems?.size ?: 0

    protected open fun getItem(index: Int): T = listItems!![index]

    fun setItems(items: List<T>) {
        listItems = items.toMutableList()
        notifyDataSetChanged()
    }

    fun getAllItems(): List<T> = listItems!!

    open fun addItems(items: List<T>?) {
        if (items != null) {
            if (listItems == null)
                listItems = mutableListOf()
            listItems?.addAll(items)
            notifyDataSetChanged()
        }
    }

    fun addItem(item: T, index: Int = 0) {
        listItems?.add(index, item)
        notifyDataSetChanged()
    }

    fun setItem(item: T, index: Int = 0) {
        listItems?.set(index,item)
        notifyDataSetChanged()
    }

    fun removeItemPosition(index: Int = 0) {
        listItems?.removeAt(index)
        notifyDataSetChanged()
    }

    fun removeItem(item: T) {
        listItems?.remove(item)
        notifyDataSetChanged()
    }

}