package es.mmm.gpsalarm.global

import android.content.Context
import android.databinding.ViewDataBinding
import android.support.v7.widget.RecyclerView

/**
 * Created by lmartin on 20/04/2018.
 */
abstract class ViewHolderAbstract<T>(val binding: ViewDataBinding) : RecyclerView.ViewHolder(binding.root) {

    abstract fun bind(context: Context, bindableObject: T)
}