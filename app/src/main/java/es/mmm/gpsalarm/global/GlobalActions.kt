package es.mmm.gpsalarm.global

interface GlobalActions {
    fun back()
    fun share()
    fun search()
}