package es.mmm.gpsalarm.stops

import android.content.Context
import android.databinding.DataBindingUtil
import android.view.LayoutInflater
import android.view.ViewGroup
import es.mmm.gps_alarm.R
import es.mmm.gps_alarm.databinding.ItemStopBinding
import es.mmm.gpsalarm.data.MetroStop
import es.mmm.gpsalarm.global.AbstractGlobalAdapter
import es.mmm.gpsalarm.global.ViewHolderAbstract

class StopsAdapter(context: Context, val stopsActions: StopsActions, listItems: MutableList<MetroStop>?) :
    AbstractGlobalAdapter<MetroStop>(context, listItems) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolderAbstract<MetroStop> {

        val layoutInflater = LayoutInflater.from(parent.context)
        val binding: ItemStopBinding =
            DataBindingUtil.inflate(layoutInflater, R.layout.item_stop,parent,false)

        return MetroStopHolder(binding)

    }
    override fun onBindViewHolder(holder: ViewHolderAbstract<MetroStop>, position: Int) {
        holder as MetroStopHolder
        holder.bind(context, stopsActions, getItem(position))
    }
}