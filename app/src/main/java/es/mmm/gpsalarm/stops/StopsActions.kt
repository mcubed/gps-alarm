package es.mmm.gpsalarm.stops

import es.mmm.gpsalarm.data.MetroStop

interface StopsActions {

    fun setStop(stop: MetroStop)
}