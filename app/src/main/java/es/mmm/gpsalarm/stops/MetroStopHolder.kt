package es.mmm.gpsalarm.stops

import android.content.Context
import es.mmm.gps_alarm.BR
import es.mmm.gps_alarm.databinding.ItemLineBinding
import es.mmm.gps_alarm.databinding.ItemStopBinding
import es.mmm.gpsalarm.data.MetroStop
import es.mmm.gpsalarm.global.ViewHolderAbstract

class MetroStopHolder(binding: ItemStopBinding): ViewHolderAbstract<MetroStop>(binding){

    override fun bind(context: Context, bindableObject: MetroStop) {
        binding.setVariable(BR.stop, bindableObject)
    }
    fun bind(context: Context, actions: StopsActions, bindableObject: MetroStop) {
        binding.setVariable(BR.stop, bindableObject)
        binding.setVariable(BR.actions, actions)
    }
}