package es.mmm.gpsalarm.stops

import android.content.Context
import android.content.SharedPreferences
import android.databinding.DataBindingUtil
import android.support.v7.widget.LinearLayoutManager
import es.mmm.gps_alarm.R
import es.mmm.gps_alarm.databinding.ActivityStopsBinding
import es.mmm.gpsalarm.Constants
import es.mmm.gpsalarm.data.MetroData
import es.mmm.gpsalarm.data.MetroStop
import es.mmm.gpsalarm.global.GlobalActivity

class StopsActivity : GlobalActivity(), StopsActions {

    private lateinit var binding: ActivityStopsBinding

    override fun initBinding() {
        binding = DataBindingUtil.setContentView(this@StopsActivity, R.layout.activity_stops)
        val data = MetroData(this)
        val line = intent.getStringExtra(Constants.LINE)

        //Recycler view
        binding.stopsRecycler.layoutManager = LinearLayoutManager(this)
        binding.stopsRecycler.adapter = StopsAdapter(this, this, data.getLineStops(line))

    }

    override fun setStop(stop: MetroStop) {
        val preferences = getSharedPreferences("defalut", Context.MODE_PRIVATE)
        preferences.edit().putString(Constants.STOP, stop.attributes.id).apply()
    }

}
