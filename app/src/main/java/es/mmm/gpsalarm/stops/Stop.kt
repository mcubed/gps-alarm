package es.mmm.gpsalarm.stops

data class Stop(val id: String, val name: String)